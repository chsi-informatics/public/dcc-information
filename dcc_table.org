

| Node Type | CPUs | GPUs | RAM    | # Nodes |
|-----------+------+------+--------+---------|
| CPU       |   84 |    0 | 700 GB |      21 |
| CPU       |   60 |    0 | 609 GB |       2 |
| GPU       |    6 |    1 | 30 GB  |       8 |





| Location        | Space  | Longterm | Speed |
|-----------------+--------+----------+-------+
| Home Directory  | 10G    | Yes      | Fast  |
| /hpc/group/chsi | 1TB    | Yes      | Fast  |
| /work           | 650T   | No       | Fast  |
| /datacommons/   | $80/TB | Yes      | Slow  |

