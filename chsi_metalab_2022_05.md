## Link to Slides

<https://duke.is/wgk4b>

## Why Duke Compute Cluster?

-   You need lot’s of:
    -   CPUs
    -   GPUs
    -   RAM
    -   working storage
-   You don’t want to tax your laptop/desktop
-   Centralized computing
-   Free\*

## Why NOT Duke Compute Cluster?

-   Learning Curve: SLURM/OOD
-   Sensitive data is NOT allowed
    -   PHI
    -   FERPA
    -   etc
-   Linux only
-   Don’t like to share

## DCC Specs

-   1300 nodes which combined have
    -   30,000 vCPUs
    -   750 GPUs
    -   200TB RAM
    -   7 PB Isilon file system
-   Interconnects are 10 Gbps or 40 Gbps
-   CentOS 8 Stream
-   SLURM is the job scheduler

## CHSI DCC Specs

| Node Type | CPUs | GPUs | RAM    | \# Nodes |
|-----------|------|------|--------|----------|
| CPU       | 84   | 0    | 700 GB | 21       |
| CPU       | 60   | 0    | 609 GB | 2        |
| GPU       | 6    | 1    | 30 GB  | 8        |

## SSH

    ssh NetID@dcc-login.oit.duke.edu

SSH Shortcuts:
<https://chsi.duke.edu/resources/chsi-cluster-user-guide#ssh-auth>

## DCC Open OnDemand

<https://dcc-ondemand-01.oit.duke.edu/>

## Why NOT Open OnDemand

Why use commandline?

-   Run SLURM jobs
    -   sbatch arrays
    -   NextFlow
    -   CWL
-   More control

## Storage

| Location        | Space  | Longterm | Speed |
|-----------------|--------|----------|-------|
| Home Directory  | 10G    | Yes      | Fast  |
| /hpc/group/chsi | 1TB    | Yes      | Fast  |
| /work           | 650T   | No       | Fast  |
| /datacommons/   | $80/TB | Yes      | Slow  |

## Useful Links

-   CHSI Cluster User Guide
    -   <https://chsi.duke.edu/resources/chsi-cluster-user-guide>
-   DCC General Help:
    -   <https://oit-rc.pages.oit.duke.edu/rcsupportdocs/dcc/>
-   DCC Open OnDemand
    -   <https://dcc-ondemand-01.oit.duke.edu/>
    -   <https://oit-rc.pages.oit.duke.edu/rcsupportdocs/OpenOnDemand/>

## How?

-   email
    -   Josh <josh@duke.edu>
    -   Scott <scott.white@duke.edu>
